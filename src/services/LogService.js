const LOG_DEBUG_PREFIX = '[debug] ';
const LOG_INFO_PREFIX = '[info] ';
const LOG_WARN_PREFIX = '[warn] ';
const LOG_ERROR_PREFIX = '[error] ';

class LogService {
  constructor(log) {
    this.log = log;
  }

  debug(message) {
    this.log(LOG_DEBUG_PREFIX + message);
  }

  info(message) {
    this.log(LOG_INFO_PREFIX + message);
  }

  warn(message) {
    this.log(LOG_WARN_PREFIX + message);
  }

  error(message) {
    this.log(LOG_ERROR_PREFIX + message);
  }
}

module.exports = LogService;
