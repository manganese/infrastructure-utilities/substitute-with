#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const LogService = require('../src/services/LogService');
const logService = new LogService(console.log);

const yargs = require('yargs')
  .scriptName('substitute-with')
  .usage('$0 <source-file>')
  .command(
    '$0',
    'the default command',
    () => {
      yargs.positional('source-file', {
        type: 'string',
        describe: 'The path to the file containing variables.',
      });

      yargs.option('variables', {
        type: 'string',
        describe: 'The path to the file containing variables.',
        requiresArg: true,
      });

      yargs.option('aws', {
        type: 'boolean',
        describe:
          'If set, will also use variables from EC2 instance user data.',
        default: false,
        requiresArg: false,
      });
    },
    (argv) => {
      // Not sure why it won't parse the positional, this is a workaround
      argv.sourceFile = argv._[0];

      let variablesFiles = [];

      if (argv.aws) {
        variablesFiles.push('/var/lib/cloud/instance/user-data.txt');
      }

      if (typeof argv.variables === 'string') {
        variablesFiles.push(argv.variables);
      } else if (Array.isArray(argv.variables)) {
        variablesFiles = variablesFiles.concat(argv.variables);
      }

      const source = fs.readFileSync(argv.sourceFile).toString();
      const variables = variablesFiles.reduce((variables, variablesFile) => {
        const variablesFileString = fs.readFileSync(variablesFile).toString();
        const variablesFileLines = variablesFileString.split('\n');
        const variablesFileVariables = variablesFileLines.reduce(
          (variablesFileVariables, line) => {
            const lineTrimmed = line.replace(/^\s+/, '');

            if (!lineTrimmed || lineTrimmed.startsWith('#'))
              return variablesFileVariables;

            const lineTokens = lineTrimmed.split('=');
            const variableKey = lineTokens[0];
            const variableValue = lineTokens.slice(1).join('=');

            return {
              ...variablesFileVariables,
              [variableKey]: variableValue,
            };
          },
          {}
        );

        return {
          ...variables,
          ...variablesFileVariables,
        };
      }, {});

      const sourceSubstituted = source.replace(
        /\$[a-z_-][a-z0-9_-]+/gi,
        (match) => {
          const variableKey = match.replace(/^\$/, '');
          const variableValue = variables[variableKey] || '';

          return variableValue;
        }
      );

      console.log(sourceSubstituted);
    }
  );

yargs.help().argv;
